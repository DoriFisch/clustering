% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cutTree.R
\name{cutTree}
\alias{cutTree}
\title{cutTree}
\usage{
cutTree(tree, height = NULL, K = NULL)
}
\arguments{
\item{tree}{a list which contains data and a clustering dendogram as
produced by \code{hierClust}}

\item{height}{a number which specifies where the tree is to be cut}

\item{K}{a number which specifies the desired number of clusters}
}
\value{
a clustered Tibble of class "clustered_tbl"
}
\description{
Is used to extract a specific clustering from a hierClust dendogram.
}
\details{
Either height or K has to be given.
}
\examples{
data <- clusterGenerator(c(1,3,6,8), 2)
x <- hierClust(data)
plot(cutTree(x, K = 4))
}
