# context("Test the dbscan function")

data <- clusterGenerator(c(40,10,60,30,70),2)
eps <-
clustered_data <- dbscan(data, 4, 1)
data_mat <- as.matrix(data)
data_with_NA <- data
data_with_NA[3:5,1] <- NA
suppressWarnings(clustered_data_with_NA <- dbscan(data_with_NA, 4, 1))

test_that("the output of dbscan is correct", {
  expect_type(clustered_data, "list")
  expect_length(clustered_data, 3)
  expect_true(all(names(clustered_data)%in%c("V1", "V2", "cluster_label")))
  expect_s3_class(clustered_data, "clustered_tbl")
  expect_equal(length(as.matrix(clustered_data)), length(data_mat[!is.na(data_mat)]) + NROW(clustered_data)) #, ignore_attr = TRUE
  expect_equal(length(as.matrix(clustered_data_with_NA)), length(as.matrix(data_with_NA[complete.cases(data_with_NA), ])) + NROW(clustered_data_with_NA))
  expect_true(all(0 <= clustered_data$cluster_label))
  expect_equal(NCOL(clustered_data_with_NA), NCOL(data)+1, ignore_attr = TRUE)
})

test_that("dbscan inputs are checked for errors", {
  expect_error(dbscan(data, -1))
  expect_type(dbscan(data, 0), "list")
  expect_error(dbscan(data, "4"))
  expect_error(dbscan(data, T))
  expect_error(dbscan(data, 4, -1))
  expect_error(dbscan(data, 4, 0))
  expect_error(dbscan(data, 4, "4"))
  expect_error(dbscan(data, 4, T))
  expect_type(dbscan(data, 4, 1.75), "list")
})

test_that("dbscan output is sensible", {
  expect_equal(length(unique(dbscan(data, NROW(data), 1)$cluster_label)), 1)
})


