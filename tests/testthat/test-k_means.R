# context("Test the k_means function")

daten <- clusterGenerator(c(40,10,60,30,70),2)
K <- 5
T <- 1000
k_means_data <- k_means(daten, K, T)
daten_mat <- as.matrix(daten)
data_with_na <- daten
data_with_na[3:5,1] <- NA
suppressWarnings(clustered_data_with_na <- k_means(data_with_na, 3, T))

test_that("the output of k_means is correct", {
  expect_type(k_means_data, "list")
  expect_length(k_means_data, 3)
  expect_true(all(names(k_means_data)%in%c("data", "cluster_center", "sum_of_distances")))
})
  #zum Tibble:
test_that("the output data is a clustered tibble", {
  expect_s3_class(k_means_data$data, "clustered_tbl")
  expect_equal(length(as.matrix(k_means_data$data)), length(daten_mat[!is.na(daten_mat)]) + NROW(k_means_data$data)) #, ignore_attr = TRUE
  expect_equal(length(as.matrix(clustered_data_with_na$data)), length(as.matrix(data_with_na[complete.cases(data_with_na), ])) + NROW(clustered_data_with_na$data))
  expect_true(all(0 < k_means_data$data$cluster_label))
  expect_true(all(k_means_data$data$cluster_label < K + 1))
  expect_equal(NCOL(clustered_data_with_na), NCOL(daten)-1, ignore_attr = TRUE)
})
  #zu den cluster_center:
test_that("the cluster_center are in a matrix", {
  expect_true("matrix" %in% class(k_means_data$cluster_center))
  expect_false(is.null(colnames(k_means_data$cluster_center)))
  expect_equal(NCOL(k_means_data$cluster_center), K, ignore_attr = TRUE)
  expect_equal(NROW(k_means_data$cluster_center), NCOL(daten))
  expect_equal(length(!is.na(k_means_data$cluster_center)), length(k_means_data$cluster_center))
})
  #zu der sum_of_distances:
test_that("the sum_of_distances is a number", {
  expect_true(typeof(k_means_data$sum_of_distances) %in% c("integer", "double"))
  expect_length(k_means_data$sum_of_distances, 1)
  expect_gte(k_means_data$sum_of_distances, 0)
  expect_vector(k_means_data$sum_of_distances, ptype = double(), size = 1)
})

K_test <- 2
m_k_ind <- sample(NROW(daten), K_test)
m_k_test <- matrix(daten_mat[m_k_ind, ], ncol = K_test, nrow = NCOL(daten), byrow = TRUE)

cluster <- vector("double",length=NROW(daten))
for (i in seq_along(cluster)) {
  dist <- apply(m_k_test, 2, sum_of_squares, x=daten_mat[i, ])
  cluster[[i]] <- which.min(dist)
}
sum_of_distances <- 0
for (k in 1:K_test) {
  data_indices_in_cluster <- which(cluster == k)
  cluster_distance <- sum(apply(daten_mat[data_indices_in_cluster, ], 1, sum_of_squares, x = m_k_test[ ,k]))
  sum_of_distances <- sum_of_distances + cluster_distance
}

test_that("k_means does not increase the sum of distances", {
  expect_lte(k_means(daten, K_test, T)$sum_of_distances, sum_of_distances)
})

# setwd("~/Dokumente/Studium/RKurs/Projekt/Projekt/clustering/tests/testthat")
# devtools::test()
# test_file("test-k_means.R")

