# density based clustering

test_that("get_neighbours works", {
  data <- matrix(c(0,0,0,1,1,0,1,1), nrow=4, byrow=T)
  expect_equal(get_neighbours(data, 1, 1.5)[[1]], c(T, T, T, T))
  expect_equal(get_neighbours(data, 1, 1.1)[[1]], c(T, T, T, F))

})
