#' Obtain clustered data from optics output
#'
#' While the \code{optics} function doesn't produce a cluster assignment, it can be
#' used to manually inspect the data's density to determine a sensible epsilon.
#' Afterwards, a clustering can be obtained by calling \code{cluster_optics}.
#' The result is similar to a clustering obtained by using DBSCAN with the same
#' parameters.
#'
#' @param optics_order Return value of \code{optics} function.
#' @param data A tibble containing continuous columns \code{x_i}; describes the
#'   observed data.
#' @param eps Threshold for the reachability distance ordering. When the next
#'   point exceeds it, a new cluster is started.
#' @return A clustered tibble of class "clustered_tbl". Contains the input
#'   tibble data with new column: \code{cluster_label}. \code{cluster_label}
#'   assigns the cluster number to each data point. The value \code{0} indicates
#'   that a data point is considered noise.
#' @examples
#' data <- clusterGenerator(c(40,10,60,30,70), 2)
#' cluster_optics(optics(data, 4, 1), data, 1.5)
#' @export
cluster_optics <- function(optics_order, data, eps=NA) {
  data <- check_data(data)
  data_mat <- as.matrix(data)
  eps <- check_eps(eps, data_mat)

  order <- optics_order[[1]]
  reach_dists <- optics_order[[2]]
  core_dists <- optics_order[[3]]

  stopifnot("Number of data points in optics_order and data are not equal. Make
            sure to use the same data as used for the optics function." =
            length(order) == NROW(data))

  row_label <- rep(0, length(order))
  current_label <- 0
  # is_noise <- F
  for (idx in 1:length(order)) {
    position <- order[idx]

    reach_dists <- replace_na(reach_dists, Inf)
    if (reach_dists[position] <= eps) {
      # position belongs to same cluster as previous point
      row_label[position] <- current_label
    } else {
      current_label <- current_label + 1
      if (is.na(core_dists[[position]]) | core_dists[[position]] > eps) {
        # there are fewer than minPts points in the eps-radius -> Noise
        row_label[position] <- 0
      } else {
        row_label[position] <- current_label
      }
    }
  }
  data %>%
    mutate(cluster_label = row_label) ->
    clustered_data
  class(clustered_data) <- c("clustered_tbl", class(data))
  return(clustered_data)
}
