#' Plotting OPTICS reachabilities
#'
#' Display a barplot of the reachabilities (on the y-axis) in the order returned
#' by the optics function (on the x-axis). The plot can be used to determine a
#' suitable eps threshold for the cluster_optics function. Visualising the
#' threshold as a horizontal line in the reachability-plot, valleys enclosed by
#' intersections of reachabilities with the threshold will constitute a cluster
#' after applying cluster_optics.
#'
#' @param optics_order Return value of the \code{optics} function.
#' @param ... Further optional arguments that will be passed to the barplot function.
#'
#' @examples
#' data <- clusterGenerator(c(40,10,60,30,70), 2)
#' output <- optics(data, 4, 5)
#' barplot(output)
#' @export
barplot.optics_order <- function(optics_order, ...) {
  # TODO: Error Checks
  ordering <- optics_order[[1]]
  dists <- optics_order[[2]]
  upper <- 1.5 * max(replace_na(dists, 0))
  # Visualize undefined ("infinite") reachability distances using the upper
  # limit of the canvas
  values <- replace_na(dists[ordering], upper)
  barplot(values, ylim=c(0,upper))
}
