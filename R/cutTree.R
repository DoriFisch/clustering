#' cutTree
#'
#' Is used to extract a specific clustering from a hierClust dendogram.
#'
#' Either height or K has to be given.
#'
#' @param tree a list which contains data and a clustering dendogram as
#' produced by \code{hierClust}
#' @param height a number which specifies where the tree is to be cut
#' @param K a number which specifies the desired number of clusters
#'
#' @return a clustered Tibble of class "clustered_tbl"
#'
#' @examples
#' data <- clusterGenerator(c(1,3,6,8), 2)
#' x <- hierClust(data)
#' plot(cutTree(x, K = 4))
#' @export
cutTree <- function(tree, height=NULL, K=NULL) {

  #check if arguments are valid
  data <- tree$data
  tree <- tree$dendrogram
  n <- nrow(tree) + 1

  data <- check_data(data)
  stopifnot("K must be a natural number not bigger than n"
            = is.null(K) | K %in% 1:n)
  if (is.null(height) & is.null(K)) K <- floor(0.5*n)

  #find K

  if (is.null(K)) {
    steps = 0
    for (i in tree[,3]) {
      if (i > height) break
      else steps <- steps + 1
    }
  }
  else steps = n-K

  #build Tree

  clusters = as.list(1:n)
  for (i in seq_len(steps)) {
    a <- tree[i,1]
    b <- tree[i,2]
    clusters <- c(list(c(clusters[[a]],clusters[[b]])), clusters[-c(a,b)])
  }

  #output

  asign <- rep(0,n)
  for (i in seq_along(clusters)) {
    asign[clusters[[i]]] <- i
  }

  clustered_data <- mutate(data, cluster_label = asign)
  class(clustered_data) <- c("clustered_tbl", class(data))

  return(clustered_data)

}


